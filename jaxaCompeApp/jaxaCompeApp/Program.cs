﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Web.Script.Serialization;


namespace jaxaCompeApp
{
    public class Data0
    {
        public string result { get; set; }
    }

    public class jaxa
    {
        const string JAXA_API = "https://joa.epi.bz/api/{0}?token=TOKEN_zzkC_";

        public string sample( string data )
        {
            string uri = string.Format( JAXA_API, "prcall" );
            uri += "&date=2012-08-01&lat=30.2&lon=130.5&range=1.0&fomat=JSON";
            string result = httpGet( uri, "" );
            

            if( result != null && result.Length > 0 )
            {
                try
                {
                    // JSON 形式データを変換
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    List<Data0> res = ser.Deserialize<List<Data0>>(result);

                }
                catch ( Exception e )
                {
                    System.Diagnostics.Debug.Print( e.ToString() );
                }
            }
            return result;
        }

        string httpGet( string URI, string Parameters )
        {
            string result = "";

            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create( URI );
                req.Method = "GET";

                System.Net.WebResponse resp = req.GetResponse();
                if( resp == null )
                {
                    return "";
                }

                System.IO.StreamReader sr = new System.IO.StreamReader( resp.GetResponseStream() );
                result = sr.ReadToEnd().Trim();
            }
            catch ( Exception e )
            {
                System.Diagnostics.Debug.Print( e.ToString() );
            }

            return result;
        }
    }

    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
